#!/usr/bin/env python3
# PYTHON_ARGCOMPLETE_OK
# -*- coding: utf-8 -*-
import datetime
import sys
import argcomplete
from mytasker.daemon import Notifications
from .parser import create_parser
from mytasker.config import Config
from .printer import (
    show_user_info, 
    show_task,
)
from .check import check_input_date
from mytasker.mytasker import MyTasker


def main():
    try:
        db = MyTasker()
        db_users = db.users
        task_user = db.tasks
        parser = create_parser()
        argcomplete.autocomplete(parser)
        namespace = parser.parse_args()

        if namespace.work == "task":

            if namespace.work_task == "show":
                if namespace.id:
                    task_with_id = task_user.search(namespace.id)
                    if task_with_id is not None:
                        show_task(task_with_id)

                elif namespace.all:
                    for task in task_user.collection_task:
                        show_task(task)

                elif namespace.tag:
                    result = []
                    task_user.task_search_by_tag(namespace.tag, result=result)
                    for task in result:
                        show_task(task, show_one_task=True)

                elif namespace.priority:
                    result = []
                    task_user.task_search_by_priority(namespace.priority, result=result)
                    for task in result:
                        show_task(task, show_one_task=True)

                elif namespace.history:
                    for task in db_users.current_user.history:
                        show_task(task)

            elif namespace.work_task == "remove":
                if namespace.user:
                    task_user.delete_user_from_task(
                        namespace.id, namespace.user)

                elif namespace.id:
                    task_user.remove(namespace.id)

            elif namespace.work_task == "add":
                if namespace.add == "task":

                    name_task = " ".join(
                        namespace.name) if namespace.name is not None else None
                    deadline_task = check_input_date(" ".join(
                        namespace.deadline)) if namespace.deadline is not None else None
                    info_task = " ".join(
                        namespace.info) if namespace.info is not None else None

                    if namespace.id is None:
                        task_user.add(name_task=name_task,
                                      time_end=deadline_task,
                                      info=info_task)
                    else:
                        task_user.add_subtasks(name_task=name_task,
                                               time_end=deadline_task,
                                               task_id=namespace.id)

                elif namespace.add == "user":

                    task_user.add_user_from_task(
                        namespace.task, namespace.user)

                elif namespace.add == "tag":
                    task_user.add_task_tag(namespace.id, namespace.tag)

            elif namespace.work_task == "change":

                if namespace.admin and namespace.id:
                    task_user.switch_admin_task(
                        namespace.admin, namespace.id)
                else:
                    deadline_task = check_input_date(" ".join(
                        namespace.deadline)) if namespace.deadline is not None else None
                    name_task = " ".join(
                        namespace.name) if namespace.name is not None else None
                    task_user.change(id=namespace.id, name_task=name_task,
                                     time_end=deadline_task,
                                     priority=namespace.priority,
                                     tag=namespace.tag,
                                     status=namespace.status)

            elif namespace.work_task == "complete":

                task_user.complete_task(
                    namespace.id)

        elif namespace.work == "user":
            if namespace.work_user == "add":
                user_name = " ".join(
                    namespace.name) if namespace.name is not None else None
                user_info = " ".join(
                    namespace.info) if namespace.info is not None else None
                db_users.add(user_name,
                             user_info)

            elif namespace.work_user == "remove":
                db_users.remove(namespace.id, task_user)

            elif namespace.work_user == "show":
                if namespace.all:
                    for user in db_users.collection_users:
                        show_user_info(user)
                else:
                    show_user_info(db_users.search(namespace.id))

            elif namespace.work_user == "switch":

                db_users.switch_user(namespace.id)

            elif namespace.work_user == "change":

                user_name = " ".join(
                    namespace.name) if namespace.name is not None else None
                user_info = " ".join(
                    namespace.info) if namespace.info is not None else None
                db_users.change(namespace.id, user_name,
                                user_info)

        elif namespace.work == "periodtask":
            if namespace.work_with_period_task == "add":
                period = datetime.timedelta(hours=namespace.hour, days=namespace.day,
                                            minutes=namespace.minute, weeks=namespace.week)
                task_user.add_period_task(
                    namespace.id, db_users.current_user, period)

            elif namespace.work_with_period_task == "remove":
                task_user.remove_period_task(
                    namespace.id, db_users.current_user)

            elif namespace.work_with_period_task == "change":
                period = datetime.timedelta(hours=namespace.hour, days=namespace.day,
                                            minutes=namespace.minute, weeks=namespace.week)
                task_user.change_period_task(namespace.id, period)

        if namespace.clearhistory:
            db_users.clear_history(db_users.current_user.id)

        if namespace.work == "notification":
            config = Config()
            path = config.get_setting('daemon_pidfile')
            if namespace.start:
                notifications = Notifications(db_users, path)
                notifications.start()

            if namespace.stop:
                notifications = Notifications(db_users, path)
                notifications.stop()
        db_users.write()
    except ValueError as ve:
        print(ve)
    except KeyError as ke:
        print(ke)
    except IOError as eo:
        print(eo)
    except Exception as ex:
        print(ex)


if __name__ == '__main__':
    main()
