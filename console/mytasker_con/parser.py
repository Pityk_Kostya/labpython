import argparse


def create_parser():
    parser = argparse.ArgumentParser()

    subparsers = parser.add_subparsers(dest='work')

    work_task = subparsers.add_parser('task', help='work with tasks')
    task_subparser = work_task.add_subparsers(dest="work_task")

    show = task_subparser.add_parser('show', help='show info tasks')
    show.add_argument('--all', '-a', action='store_const', const=True,
                      default=False,
                      help='show all tasks')
    show.add_argument('--id', '-id', type=int, default=None,
                      help='show task with id', metavar='id')
    show.add_argument('-t', '--tag', type=str, help='show task with tag')
    show.add_argument('-p', '--priority', type=int, help='show task priority')
    show.add_argument('-ht', '--history', action='store_const', const=True,
                      default=False, help='show task history')

    delete = task_subparser.add_parser('remove', help='Remove task')
    delete.add_argument('--id', '-id', type=int, help='Remove task id')
    delete.add_argument('-u', '--user', type=int,
                        help='Remove task from users(if you admin task)')

    add = task_subparser.add_parser('add', help='add: Task, SabTask or User')
    what_add = add.add_subparsers(dest='add')

    task_add = what_add.add_parser('task', help='add task')
    task_add.add_argument('--name', '-n', type=str, default=None,
                          nargs="+", help="Name Task")
    task_add.add_argument('--deadline', '-d', type=str, default=None,
                          nargs="+", help="Task Deadline")
    task_add.add_argument('--info', '-inf', type=str,
                          nargs="+", default=None, help="Info Task")
    task_add.add_argument('-id', '-id', type=int,
                          default=None, help="id task, with add subtask")

    tag = what_add.add_parser('tag', help="add task tag")
    tag.add_argument('-id', '--id', type=int, help="id task with add tag")
    tag.add_argument('-t', '--tag', type=str,
                     help="added task tag", default=None)

    add_user_from_task = what_add.add_parser('user', help="add user from task")
    add_user_from_task.add_argument('-t', '--task', type=int, default=None,
                                    help='id task')
    add_user_from_task.add_argument('-u', '--user', type=int, default=None,
                                    help='id user')

    change_task = task_subparser.add_parser('change', help="change params task")
    change_task.add_argument('--id', '-id', type=int, help="change task id")
    change_task.add_argument('--name', '-n', type=str, default=None, nargs="+",
                             help="change task name")
    change_task.add_argument('--deadline', '-d', type=str, nargs="+", default=None,
                             help="change task deadline")
    change_task.add_argument('--priority', '-pr', type=int, default=0,
                             help="change task priority")
    change_task.add_argument('--tag', '-t', type=str, default=None,
                             help="change task tag")
    change_task.add_argument('--status', '-st', type=str,
                             default=None, help="change task status")
    change_task.add_argument('--admin', '-a', type=int, default=None,
                             help="change admin task")

    comlete = task_subparser.add_parser('complete', help="Complite task")
    comlete.add_argument('--id', '-id', type=int, default=None,
                         help="id task, whar will be complite")

    work_user = subparsers.add_parser('user',  help="work with user")
    user_subparser = work_user.add_subparsers(dest="work_user")

    add_user = user_subparser.add_parser('add', help="add user")
    add_user.add_argument('-n', '--name', nargs="+", type=str,
                          default=None, help="add user name")
    add_user.add_argument('-info', '--info', nargs="+", type=str, default=None,
                          help="add user info")
    show_users = user_subparser.add_parser('show', help="show user info")
    show_users.add_argument('-a', '--all', action='store_const', const=True,
                            default=False, help="show all users")
    show_users.add_argument('-id', '--id', type=int,
                            help="show user with id", metavar="id")

    delete_user = user_subparser.add_parser('remove', help="Remove user")
    delete_user.add_argument('-id', '--id', type=int, help="Remove user id")

    switch_user = user_subparser.add_parser('switch', help="Switch user")
    switch_user.add_argument('-id', '--id', type=int,
                             help="Switch current user from user with id")

    change_user = user_subparser.add_parser('change', help="Correct user info")
    change_user.add_argument('-id', '--id', type=int, help=" Correct user id")
    change_user.add_argument('-n', '--name', type=str, nargs="+", default=None,
                             help=" Correct user name")
    change_user.add_argument('-inf', '--info', type=str, default=None,
                             nargs="+", help=" Correct user info")

    parser.add_argument('-ch', '--clearhistory', action='store_const', const=True, default=False,
                        help="Clear history current user")

    work_with_period_task = subparsers.add_parser('periodtask',
                                                  help="work with Period Task")

    period_task = work_with_period_task.add_subparsers(dest='work_with_period_task')

    add_period_task = period_task.add_parser('add', help="Edit task to period task")
    add_period_task.add_argument('-id', '--id', type=int, default=0,
                                 help="id task to edit on period task")
    add_period_task.add_argument('-m', '--minute', type=int, default=0,
                                 help="minute in period")
    add_period_task.add_argument('-р', '--hour', type=int, default=0,
                                 help="hour in period")
    add_period_task.add_argument('-d', '--day', type=int, default=0,
                                 help="day in period")
    add_period_task.add_argument('-w', '--week', type=int, default=0,
                                 help='weeks in period')

    remove_period_task = period_task.add_parser('remove', help="Remove period on task ")
    remove_period_task.add_argument('-id', '--id', type=int, help="id on period task")

    change_period_task = period_task.add_parser('change', help="change period task")
    change_period_task.add_argument('-id', '--id', type=int, default=0,
                                    help="change task id")
    change_period_task.add_argument('-m', '--minute', type=int, default=0,
                                    help="change minute in period")
    change_period_task.add_argument('-o', '--hour', type=int, default=0,
                                    help="change hour in period")
    change_period_task.add_argument('-d', '--day', type=int, default=0,
                                    help="change day in period")
    change_period_task.add_argument('-w', '--week', type=int, default=0,
                                    help='change weeks in period')

    notification = subparsers.add_parser('notification', help="notification with period task")

    notification.add_argument('-start', '--start', action='store_const', const=True,
                              default=False,
                              help="Started work notification")
    notification.add_argument('-stop', '--stop', action='store_const', const=True,
                              default=False,
                              help="Stoped work notification")

    return parser
