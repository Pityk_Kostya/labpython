from datetime import datetime



def show_task(task=None, deep=0, show_one_task=False):
        """
        Show task info.
        Recursive method: if task have subtasks,call this method on all subtasks

        Keyword arguments:
            task -- object task that will be show.
            deep -- deep, if task have subtask (default = 0)
            show_one_task -- bool flag: show one task or task 
            and subtasks (default = False)
        """
        info = "\t" * deep + "Id:" + str(task.id) + "| name:" + str(
            task.name) + "| Start - End:" + str(
            datetime.strftime(task.time_start, "%d.%m.%Y %H:%M")) + " - " + str(
            datetime.strftime(task.time_end, "%d.%m.%Y %H:%M")) + "| Status:" + str(
            task.status) + "| Teg:" + str(
            ', '.join(task.tag) if len(task.tag) != 0 else "None") + "| Admin:" + str(task.admin_task)
        print(info)
        if task.subtasks and not show_one_task:
            deep += 1
            for sub in task.subtasks:
                show_task(sub, deep)

        

def show_user_info(object_user):
        """Show user info.
        
        Keyword arguments:
            object_user -- object user that will be show.
        """ 
        print(f"Name: {object_user.name_user} id:  {object_user.id}")
        if object_user.tasks:
            print("Task: ")
            for task in object_user.tasks:
                show_task(task,deep=1)
        if not len(object_user.history) == 0:
            print("History:")
            for history in object_user.history:
                show_task(history,deep=1)
    
