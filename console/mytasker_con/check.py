
from datetime import datetime


def check_input_date(input_date):
    """
    Check format input date when users add task or subtask.

    Keyword arguments:
        input_date (str) - input date.
    """
    constant_date_format = "%d.%m.%Y %H:%M"
    temp_input_date = datetime.strptime(input_date, constant_date_format)
    if input_date != temp_input_date.strftime(constant_date_format):
        raise ValueError('invalid date format')
    if temp_input_date < datetime.now():
        raise ValueError(
            'invalid date format, date can not be in the past')

    return datetime.strptime(input_date, constant_date_format)
