from setuptools import setup


setup(
    name='mytasker_con',
    version='1.0',
    packages=[
        'mytasker_con'
    ],
    entry_points={
        'console_scripts': [
            'mytasker_con = mytasker_con.__main__:main'
        ]
    },
    include_package_data=True
)