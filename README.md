#Welcome 
This project is a library for working with your tasks and developing your task trackers.
Also contains an example of the console and web versions

### Installing 
* Download:

`git clone https://Pityk_Kostya@bitbucket.org/Pityk_Kostya/labpython.git`

* Go to **library** repository and install:

`sudo python3 setup.py install`

Library automatically create user **Guest**.
You can user your own `settings.ini` file, for this change default pass in 'configure.py'.

* To install the console interface, go to **console** repository and install:

`sudo python3 setup.py install`

## Code examples
```python
from mytasker.mytasker import MyTasker
mytasker_instance = MyTasker()
mytasker_instance.users.add('test_user')
```
To see all functions use `help(mytasker)`.

## Tests 
To run tests go to tests folder and run `python3 -m unittests test*`.

## Authors
* Kostya Pityk - **BSUIR**