from django.conf.urls import include, url
from . import views
import django.contrib.auth.views as auth_views
from django.views.generic import RedirectView

task_patterns = [
    url(r'^$', views.TaskListView.as_view(), name='all'),
    url(r'^add/', views.TaskCreateView.as_view(), name='add'),
    url(r'^history/', views.TaskHistoryVies.as_view(), name='history'),
    url(r'^(?P<pk>\d+)/$', views.TaskWorkView.as_view(), name='work'),
    url(r'^(?P<pk>\d+)/delete/$', views.delete_task_view, name='delete'),
    url(r'^(?P<pk>\d+)/complete/$', views.complete_task_view, name='complete'),
    url(r'^(?P<pk>\d+)/edit/$', views.TaskEditView.as_view(), name='edit'),
    url(r'^(?P<pk>\d+)/add_user', views.add_user_from_task, name='add_user'),
    url(r'^(?P<pk>\d+)/delete_user/(?P<name_user>\w+)/$',
        views.delete_user_from_task, name='delete_user'),
    url(r'search/',
        views.search, name='search'),
    url(r'^(?P<pk>\d+)/revive/$', views.revive_the_task, name='revive'),
    url(r'^(?P<pk>\d+)/clear/$', views.clear_task, name='clear'),
    url(r'^(?P<pk>\d+)/add-subtask/$', views.CreateSubtaskView.as_view(), name='add-subtask')                                
]
plan_patterns = [
    url(r'^$', views.PlanTaskListView.as_view(), name='all_plantask'),
    url(r'^add/', views.PlanTaskCreateView.as_view(), name='add_plantask'),
    #correct to info 
    url(r'^(?P<pk>\d+)/$', views.PlanTaskWorkView.as_view(), name='work'),
    url(r'^(?P<pk>\d+)/edit/$', views.PlanTaskEditView.as_view(), name='edit'),
    url(r'^(?P<pk>\d+)/delete/$', views.delete_plantask_view, name='delete'),
]
urlpatterns = [
    url(r'^$', RedirectView.as_view(url='tasks'), name='homepage'),
    url(r'^login/$', auth_views.LoginView.as_view(redirect_authenticated_user=True), name='login'),
    url(r'^logout/$', auth_views.logout,
        {'next_page': 'login'}, name='logout'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^tasks/', include(task_patterns, namespace='tasks')),
    url(r'^plantask/', include(plan_patterns, namespace='plantask')),
]
