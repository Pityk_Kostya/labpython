from mytasker.models import (
    Task,
    PlanTask
)
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login
from django.views.generic import (
    ListView,
    CreateView,
    DetailView,
    UpdateView,
    DeleteView,
)
from django.contrib.auth.decorators import login_required
from .forms import *
from django.contrib.auth.mixins import LoginRequiredMixin
from mytasker.db_manage import Manage
from django.urls import reverse_lazy


db = Manage()


def signup(request):
    if request.user.is_authenticated:
        return redirect('homepage')
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('homepage')
    else:
        form = UserCreationForm()
    return render(request, 'registration/signup.html', {'form': form})


#####################################################


class TaskCreateView(LoginRequiredMixin, CreateView):
    form_class = CreateTaskForm
    template_name = 'master/add_task.html'
    model = Task

    def get_form_kwargs(self):
        kwargs = super(TaskCreateView, self).get_form_kwargs()
        tasks = db.find_tasks(self.request.user.username, general=False)
        kwargs.update({'tasks': tasks})
        return kwargs

    def form_valid(self, form):
        username = self.request.user.username
        new_task = form.save(commit=False)
        new_task.owner = db.get_users(self.request.user.username)
        new_task.save()
        parent_task = form.cleaned_data['subtasks']
        parent_task.add_subtask(new_task) if parent_task else db.add_created(
            username, 'task', new_task)
        if parent_task:
            for owner in parent_task.other_user_task.all():
                add_user_from_task(self.request, new_task.pk, owner)
        return redirect('homepage')


class TaskListView(LoginRequiredMixin, ListView):
    template_name = 'master/home_page.html'
    model = Task
    context_object_name = 'tasks'

    def get_queryset(self):
        username = self.request.user.username
        return db.find_tasks(username)


class TaskHistoryVies(LoginRequiredMixin, ListView):
    template_name = 'master/home_page.html'
    model = Task
    context_object_name = 'tasks'

    def get_queryset(self):
        username = self.request.user.username
        tasks = db.find_tasks(username, history=True, general=False)
        return tasks


class TaskWorkView(LoginRequiredMixin, DetailView):

    model = Task
    template_name = 'master/work_with_task.html'
    content_object_name = 'task'

    def get_object(self, queryset=None):
        task = db.find_tasks(
            username=self.request.user.username, task_id=self.kwargs['pk'])
        return task


class TaskEditView(LoginRequiredMixin, UpdateView):
    model = Task
    template_name = 'master/add_task.html'

    fields = ['task_name', 'task_info',
              'time_end',  'tags', 'task_priority', 'status']

    def get_success_url(self):
        return reverse_lazy('tasks:work', args=(self.object.pk,))


class CreateSubtaskView(CreateView):
    form_class = CreateSubtaskForm
    template_name = 'master/add_task.html'

    def get_form_kwargs(self):
        kwargs = super(CreateSubtaskView, self).get_form_kwargs()
        parent_task = db.find_tasks(self.request.user.username, self.kwargs['pk'])
        kwargs.update({'subtasks': parent_task})
        return kwargs

    def form_valid(self, form):
        username = self.request.user.username
        parent_task = db.find_tasks(username, self.kwargs['pk'])
        new_task = form.save(commit=False)
        new_task.owner = db.get_users(self.request.user.username)
        new_task.save()
        parent_task.add_subtask(new_task)
        return redirect('tasks:work', self.kwargs['pk'])


def delete_task_view(request, pk):
    username = request.user.username
    db.delete_task(username, pk)
    return redirect('homepage')


def complete_task_view(request, pk):
    username = request.user.username
    db.complete_task(username, pk)
    return redirect('homepage')


@login_required
def add_user_from_task(request, pk, user=None):
    
    form = TaskAddUserForm(request.POST or None)
    if user:
        username = db.get_users(username=request.user.username)
        task_to_share = db.find_tasks(username, pk)
        add(username, task_to_share, user)
    else:
        if request.method == 'POST':
            if form.is_valid():
                username = db.get_users(username=request.user.username)
                user_to = db.user_to_add_task(username=form.cleaned_data['user_to'])
                if user_to is not None:
                    task_to_share = db.find_tasks(username, pk)
                    add(username, task_to_share, user_to)
                    return redirect('tasks:work', pk)
                else:
                    form = TaskAddUserForm( None)
                    form.fields['user_to'].widget.attrs['placeholder'] = 'Incorrect input name'
                
        return render(request, 'master/add_user_from_task.html', {'form': form})


def add(username, task_to_share, user_to):
    user_to.add_task_from_user(task_to_share)
    if task_to_share.sub.all().exists():
        for subtask in task_to_share.sub.all():
            add(username, subtask, user_to)


@login_required
def delete_user_from_task(request, pk, name_user):
    username = request.user.username
    db.delete_user_from_task(username, pk, name_user)
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


@login_required
def search(request):
    username = request.user.username
    if request.method == "GET":
        name_task = request.GET['task_search']
        tasks = db.find_tasks(username, tags=name_task, general=False)
        return render(request, 'master/home_page.html', {'tasks': tasks})


@login_required
def revive_the_task(request, pk):
    username = request.user.username
    db.revive_task(username, pk)
    return redirect('tasks:history')


def clear_task(request, pk):
    username = request.user.username
    db.db_remove_task(username, pk)
    return redirect('tasks:history')


###########################################################


class PlanTaskCreateView(LoginRequiredMixin, CreateView):
    model = PlanTask
    form_class = PlanModelForm
    template_name = 'master/add_task.html'

    def form_valid(self, form):
        username = self.request.user.username
        new_plan = form.save(commit=False)
        new_plan.save()
        db.add_created(username, 'plan', new_plan)
        return redirect('homepage')


class PlanTaskWorkView(LoginRequiredMixin, DetailView):

    model = PlanTask
    template_name = 'master/work_with_task.html'
    content_object_name = 'plantask'

    def get_object(self, queryset=None):
        plantask = db.find_plantask(
            username=self.request.user.username, plan_id=self.kwargs['pk'])
        return plantask


class PlanTaskEditView(LoginRequiredMixin, UpdateView):
    model = PlanTask
    form_class = PlanModelForm
    template_name = 'master/add_task.html'
   
    def get_success_url(self):
        return reverse_lazy('plantask:all_plantask')


class PlanTaskListView(LoginRequiredMixin, ListView):
    template_name = 'master/home_page.html'
    model = PlanTask
    context_object_name = 'plans'

    def get_queryset(self):
        username = self.request.user.username
        return db.find_plantask(username)


def delete_plantask_view(request, pk):
    username = request.user.username
    db.delete_plantask(username, pk)
    return redirect('plantask:all_plantask')