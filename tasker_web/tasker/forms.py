from mytasker.models import( 
    Task,
    PlanTask
)
from django import forms
import datetime
from mytasker.constants import PRIORITIES


class DateTimeLocalInput(forms.DateTimeInput):
    def format_value(self, value):
        if isinstance(value, datetime.datetime):
            return str(value.strftime('%Y-%m-%d %H:%M'))
        return str(value)


class CreateTaskForm(forms.ModelForm):
    subtasks = forms.ModelChoiceField(label='Parent task',
                                      queryset=Task.objects.none(),
                                      required=False)
    time_end = forms.DateTimeField(required=False,
                                   widget=DateTimeLocalInput(attrs={'type': "datetime-local"}),
                                   input_formats=['%Y-%m-%dT%H:%M'])
    task_priority = forms.ChoiceField(choices=PRIORITIES)

    def __init__(self, tasks, *args, **kwargs):
        super(CreateTaskForm, self).__init__(*args, **kwargs)
        if tasks:
            self.fields['subtasks'].queryset = tasks

    class Meta:
        model = Task
        fields = ['task_name', 'task_info',
                  'time_end',  'tags', 'task_priority']

class PlanModelForm(forms.ModelForm):
    period = forms.IntegerField()
    deadline = forms.DateTimeField(required=False,
                                   widget=DateTimeLocalInput(attrs={'type': "datetime-local"}),
                                   input_formats=['%Y-%m-%dT%H:%M'])

    class Meta:
        model = PlanTask
        fields = ['task_name', 'info', 'deadline', 'period_type','period']

class TaskAddUserForm(forms.Form):
    user_to = forms.CharField(label='User Name',
                                widget=forms.TextInput(attrs={'placeholder': 'Input Name User'}))


class CreateSubtaskForm(forms.ModelForm):

    time_end = forms.DateTimeField(required=False,
                                   widget=DateTimeLocalInput(attrs={'type': "datetime-local"}),
                                   input_formats=['%Y-%m-%dT%H:%M'])
                                   
    def __init__(self, *args, **kwargs):
        self._parent_task = kwargs.pop('subtasks')
        super(CreateSubtaskForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Task
        fields = ['task_name', 'task_info',
                  'time_end',  'tags', 'task_priority']

