import operator
from functools import reduce

from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from mytasker.logger import (
    logg,
    configure_logger, 
)
from mytasker.config import Config

from .models import User


class Manage:
    """Represents work with tasks and  users from database"""

    def __init__(self):
        self.config = Config()

        log_path = self.config.get_setting('logging_path')
        log_level = self.config.get_setting('logging_level')
        log_format = self.config.get_setting('logging_format')
        configure_logger(log_path, log_format, log_level)


    @staticmethod
    def get_users(username=None):
        """
        Return user or set of users
            username: name of user to return. if not exist create new   
        """
        if username:
            user = User.objects.get_or_create(name=username)[0]
            return user
        else:
            return User.objects.all()
    
    @staticmethod
    def user_to_add_task(username):
        """
        Return user or set of users
        Keyword arguments:
            username: name of user to return. 
        Raises:
            DoesNotExist: if not exist
        """
        try:
            user = User.objects.get(name=username)
            return user
        except User.DoesNotExist:
            return None

    @logg('Add task from user or plan')
    def add_created(self, username, type, object):
        """
        Add task or plantast from user.
        Keyword arguments:
            username: name of user. 
            type: task or plan
            object: object task or plan
        """
        user = self.get_users(username)
        if type == 'task':
            user.add_task(object)
        elif type == 'plan':
            user.add_plan(object)

    @logg('Find Task')
    def find_tasks(self, username, task_id=None, tags=None, history=False, general=True):
        """
        Return set of tasks of user
        Keyword arguments:
            username(str): user which tasks will be returned
            task_id(int): ID of task to return. If not found raises ObjectNotExists 
            tags(str): filter tasks by tags
            history(bool): if True returns tasks which have been completed or deleted
            general(bool): return tasks without parents
        """
        user = self.get_users(username)
        if task_id:
            try:
                return user.tasks.get(pk=task_id)
            except ObjectDoesNotExist:
                return user.other_user_task.get(pk=task_id)
        elif tags:
            result = user.tasks.filter(reduce(operator.and_, (Q(tags__contains=tag) for tag in tags)) | Q(tags=tags))

        elif history:
            result = user.tasks.filter(
                Q(status='Complete') | Q(status='Remove'))
        else:
            result = user.tasks.exclude(
                Q(status='Complete') | Q(status='Remove')) | user.other_user_task.exclude(Q(status='Complete') |
                                                                                          Q(status='Remove'))
        return result.filter(subtasks__isnull=True).order_by('time_end') if general else result.order_by('time_end')

    @logg('Find plan')
    def find_plantask(self, username, plan_id=None):
        """
        Return set of plantasks of user
        Keyword arguments:
            username(str): user which plantasks will be returned
            plan_id(int): ID of task to return.If plan_id is None, it returns all user plantasks
        """
        user = self.get_users(username)
        return user.plans.get(pk=plan_id) if plan_id else user.plans.all()

    @logg('Delete plan task')
    def delete_plantask(self, username, task_pk):
        """
        Delete plantask.
        Keyword arguments:
            username(str): user which plantask will be delete
            task_pk(int): ID of task that will be deleted
        """
        task = self.find_plantask(username, task_pk)
        task.remove_task()

    @logg('Revive task')
    def revive_task(self, username, task_pk):
        """
        Revive task.
        Keyword arguments:
            username(str): user which task will be revive
            task_pk(int): ID of task that will be revive
        """
        task = self.find_tasks(username, task_pk)
        task.revive() 

    @logg('Remove task from db')
    def db_remove_task(self, username, task_pk):
        """
        Remove task from database.
        Keyword arguments:
            username(str): user which task will be remove
            task_pk(int): ID of task that will be remove
        """
        task = self.find_tasks(username, task_pk)
        task.delete_from_history()
    
    @logg('Delete user from task')
    def delete_user_from_task(self, username, task_pk, name_user):
        """
        Delete user from task.
        Keyword arguments:
            username(str): user which admin task
            task_pk(int): ID of task that will be remove in user
            name_user(str): user that will be delete from task
        """
        task = self.find_tasks(username, task_pk)
        owner = self.find_tasks(name_user)
        owner.delete_task_from_user(task)

    @logg('Complete task')
    def complete_task(self, username, task_pk):
        """
        Complete task.
        Keyword arguments:
            username(str): user which task will be complete
            task_pk(int): ID of task that will be complete
        """
        task = self.find_tasks(username, task_pk)
        task.complete_task()

    @logg('Delete task')
    def delete_task(self, username, task_pk):
        """
        Delete task.
        Keyword arguments:
            username(str): user which task will be delete
            task_pk(int): ID of task that will be delete
        """
        task = self.find_tasks(username, task_pk)
        task.delete_task()