class User:
    """Main class User. Contains the main fields describing the user."""

    def __init__(self, id, name, info=None):
        """
        The constructor of the class User.

        Keyword arguments:
            id (int) : User id.
            name (str) : User name.
            info (str, optional) : Information about user
                (default=None)
        """
        self.id = id
        self.name_user = name
        self.user_info = info
        self.tasks = []
        self.history = []
        self.planned_tasks = []

    def delete_task(self, task_id, all_task=None):
        """Delete task on user tasks.

        Keyword arguments:
            task_id -- id remove task
            all_task -- check id, in task.subtasks (default = None)
        """
        if all_task is None:
            all_task = self.tasks
        for task in all_task:
            if task.id == task_id:
                all_task.remove(task)
                return
            elif task.subtasks:
                self.delete_task(task_id, task.subtasks)

    def delete_user_from_task(self, task_id=None, user_id=None, all_task=None):
        """Delete user from task.

        Keyword arguments:
            task_id -- id task thar remove user(task_id = None)
            user_id -- id user that must be remove on tasks (user_id = None)
            all_task -- check id, in task.subtasks (default = None)
        """

        if all_task is None:
            all_task = self.tasks
        for task in all_task:
            if task_id is None and user_id in task.users:
                task.users.remove(user_id)
            elif task.id == task_id:
                task.users.remove(user_id)
                task_id = None
            if task.subtasks:
                self.delete_user_from_task(task_id, user_id, task.subtasks)
