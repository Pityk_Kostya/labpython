from datetime import datetime
from mytasker.constants import _Status


class Task:
    """Main class Task. Contains the main fields describing the task."""

    def __init__(self, id, name, time_end, admin_task, users, info=None):
        """
        The constructor of the class Task.

        Keyword arguments:
            id (int) : Task id.
            name (str) : Task name.
            time_end (:obj: 'datetime') : Task deadline.
            admin_task (str) : Admin task.
            users (list) : List users who have this task.
            info (str, optional) : Information about task
                (default=None)
        """
        self.id = id
        self.name = name
        self.time_start = datetime.now()
        self.time_end = time_end
        self.status = _Status.CREATE
        self.admin_task = admin_task
        self.users = users
        self.task_priority = None
        self.subtasks = []
        self.task_info = info or "None"
        self.tag = []
        self.period = None