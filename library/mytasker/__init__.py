"""MyTasker Library

A library from which you can  working with your tasks and developing your task trackers. 
There is also support for users and transfer tasks between them. On the github repository
https://Pityk_Kostya@bitbucket.org/Pityk_Kostya/labpython.git` you can find examples of
integrating this library into a console or web application.


    First steps:
    >>> from mytasker import MyTasker
    >>> my_todo = MyTasker()

    MyTasker is main object of your application. When you create new instance
    of application config file and logging file creates automatically. To work
    with this application you must have at least one user.

    Usage:
    Below is a brief overview of the functionality of the library, for detailed
    use, use the help() function for the library modules.

    In the following examples of using the application, we will work with the
    object created in the section below.


        Users:
        The first instance you need to create for using this library. Through the id
        of the user you will get access to all data.

        To create user you need to:
        >>> my_todo.users(name='new_user', info='info about user')
        This function takes 2 required arguments - it is users name and user info using
        which you will work with application.
        For more details use help(management_user) or help(user).


        Tasks:
        The whole essence of this application lies in the tasks. You can do
        anything with them: create, delete, edit, transfer to users for shared
        deletion, move to archive, by id .

        To create task you need to:
        >>> my_todo.tasks.add(name_task, time_end, info)
        This function takes 3 required arguments: name_task - name create task,
        temi_end - the date when the task should be completed   and info - description 
        of this task.

        For more details use help(management_task) or help(task).

"""