import configparser
import os


class Config:
    """Class Config. Work with settings libraries."""

    def __init__(self):
        self.path = 'tmp/settings.ini'
        self.section = 'Settings'
        if not os.path.exists(self.path):
            self.create_config()

    def create_config(self):
        """Create a config file with default settings"""
        config = configparser.ConfigParser()
        config.add_section(self.section)
        config.set(self.section, "database_path", "tmp/database.json")
        config.set(self.section, "daemon_pidfile", "/tmp/daemon-example.pid")
        config.set(self.section, "tempfile_path", "tmp/temp.json")
        config.set(self.section, 'logging_path', 'tmp/logging.log')
        config.set(self.section, 'logging_level', 'INFO')
        config.set(self.section, 'logging_format', '%%(asctime)s %%(levelname)s %%(message)s')
        if not os.path.exists('tmp'):
            os.mkdir('tmp')
        with open(self.path, 'w') as config_file:
            config.write(config_file)

    def get_setting(self, key):
        """
        Return value of setting
        
        Keyword arguments:
            key(string): name of the field what value should be returned
        """

        config = configparser.ConfigParser()
        config.read(self.path)
        value = config.get(self.section, key)
        return value

    def add_setting(self, field):
        """
        Add new field to config
        Args:
            field(str): name of field
        """
        config = configparser.ConfigParser()
        config.read(self.path)
        config.set(self.section, field, '')
        with open(self.path, 'w') as config_file:
            config.write(config_file)

    def set_setting(self, field, field_value):
        """
        Set value of field
        Args:
            field(str):  name of the field what value should be set
            field_value(str): value what will be putted to the field
        """
        config = configparser.ConfigParser()
        config.read(self.path)
        config.set(self.section, field, field_value)
        with open(self.path, 'w') as config_file:
            config.write(config_file)
