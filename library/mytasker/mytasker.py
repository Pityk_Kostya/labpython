from mytasker.management_task import ManageTask
from mytasker.management_user import ManageUser


class MyTasker:
    """Represents MyTasker application instants"""
    
    def __init__(self):
        self.users = ManageUser()
        self.users.read()
        self.tasks = ManageTask(self.users.current_user.tasks, self.users)
