import json
import jsonpickle

from mytasker.config import Config
from mytasker.logger import (
    configure_logger, 
    logg,
)

from .user import User


class ManageUser:
    """The main class of work with users. Contains the basic methods for
        working with users."""

    def __init__(self, collection_users=[], path=None):
        """
        The constructor of the class ManageUser.

        Keyword arguments:
            collection_users (list, optional) : A database
                users.
            path (str, optional) : A path to database
                (default=None).
        """
        self.collection_users = collection_users
        self.current_user = None
        self.last_user_id = 0
        self.last_task_id = 0
        self.config = Config()
        self.path = path or self.config.get_setting("database_path")

        log_path = self.config.get_setting('logging_path')
        log_level = self.config.get_setting('logging_level')
        log_format = self.config.get_setting('logging_format')
        configure_logger(log_path, log_format, log_level)

    @logg('Add task')
    def add(self, name, info=None):
        """Adds a new user to the user database.

        Keyword arguments:
            name (str) -- new user name
            info (str) -- information about user (default None)
        Raises:
            ValueError: Name user is None
        """
        if name is None:
            raise ValueError("Name user is None")

        self.last_user_id += 1
        new_user = User(id=self.last_user_id, name=name, info=info)
        self.current_user = new_user
        self.collection_users.append(new_user)
    
    @logg('Removed user')
    def remove(self, id, all_task):
        """Remove user in the user database.

        Keyword arguments:
            id (int) -- user id
            all_task -- all user task
        Raises:
            ValueError : incorrect id input(not a number)
            ValueError : the user has no tasks
            ValueError: incorrect id, no user with id
        """
        if id < 0 or id > self.last_user_id:
            raise ValueError('incorrect id, no user with id %s' % id)
        elif not int(id):
            raise ValueError('incorrect id input(not a number')
        elif all_task is None or all_task is []:
            raise ValueError('the user has no tasks ')

        for user in self.collection_users:
            if user.id == id:
                for task in user.tasks:
                    all_task.search(task.id).users.remove(user.id)
                if user.id == self.current_user.id:
                    self.current_user = None
                self.collection_users.remove(user)
       
    @logg('Search user')
    def search(self, id):
        """Search user in the user database.
        Keyword arguments:
            id (int) -- user id
        Returns:
            If the user is in the user database returns a user object.
            Else return None
        Raises:
            ValueError : incorrect id input(not a number)
            ValueError : user not found
        """
        if id is None:
            raise ValueError('incorrect id input(not a number)')
        elif not int(id):
            raise ValueError('incorrect id input(not a number')
        elif id < 0 or id > self.last_user_id:
            raise ValueError('incorrect id, no user with id %s' % id)

        for user in self.collection_users:
            if user.id == id:
                return user
        return None

    @logg('Change user')
    def change(self, id, name=None, info=None):
        """Change user in the user database.

        Keyword arguments:
            id (int) -- user id
            name (str) -- new user name (default None)
            info (str) -- information about user (default None)
        Raises:
            ValueError : Incorrect id user.
            ValueError : Name user is None.

        """
        if id < 0 or id > self.last_user_id:
            raise ValueError('incorrect id, no user with id %s' % id)
        if name is None:
            raise ValueError('name is None, incorrect input')

        change_user = self.search(id)
        change_user.name_user = name or change_user.name_user
        change_user.user_info = info or change_user.user_info

    @logg('Write database')
    def write(self, path=None):
        """Write a users database in json file.
        Keyword arguments:
            path (str, optional) -- path json file (default = 'database.json')
        """
        if path is None:
            path = self.path
        with open(path, "w") as file:
            json_obj = json.dumps(json.loads(
                jsonpickle.encode(self)), indent=4)
            file.write(json_obj)

    @logg('Read database')
    def read(self, path=None):
        """Read a users database on json file.
        Keyword arguments:
            path (str, optional) -- path json file (default = 'database.json')
        """

        if path is None:
            path = self.path
        try:
            file = open(self.path, 'r')
        except IOError:
            self.add("Guest", "Temp user")
            self.write()
            
        with open(path, "r") as file:
            json_str = file.read()
            try:
                if json != "null":
                    data = jsonpickle.decode(json_str)
                    self.collection_users = data.collection_users
                    self.current_user = data.current_user
                    self.last_user_id = data.last_user_id
                    self.last_task_id = data.last_task_id
            except IOError:
                raise IOError('File is empty')

    @logg('Switch user')
    def switch_user(self, user_id):
        """Switch current user in the users database on new user.

        Keyword arguments:
            user_id (int) -- new current user id
        Raises:
            KeyError : User not found.
        """
        if user_id is not self.current_user.id:
            if self.last_user_id < user_id or user_id < 0:
                raise KeyError('Not user id %s' % user_id)
            else:
                self.current_user = self.search(user_id)

    @logg('Clear history user')
    def clear_history(self, user_id):
        """Clear user history.

        Keyword arguments:
            user_id (int) -- id user
        """
        self.search(user_id).history.clear()
