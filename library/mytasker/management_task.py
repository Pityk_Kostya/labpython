import copy

from datetime import datetime

from mytasker.constants import _Status
from mytasker.logger import logg

from .task import Task


class ManageTask:
    """The main class of work with tasks. Contains the basic methods for 
        working with tasks."""

    def __init__(self, collection_task=[], all_users=[]):
        """
        The constructor of the class MangeTask.

        Keyword arguments:
            collection_task (list, optional) : A collection of
                user tasks.
            all_users (list, optional) : A database users.
        """
        self.collection_task = collection_task
        self.all_users = all_users

    @logg('Add task')
    def add(self, name_task, time_end, info):
        """
        Add task.

        Keyword arguments:
            name_task (str) : name add task.
            time_end (:obj: 'datetime') :  deadline add task.
            info (str) : task information.

        Raises:
            ValueError : Name is None.
            ValueError : Time_end is None.
        """
        if name_task is None:
            raise ValueError('incorrect input, name is None')
        elif time_end is None:
            raise ValueError('incorrect input, deadline is None')

        self.all_users.last_task_id += 1
        admin_task = self.all_users.search(
            self.all_users.current_user.id)
        temp = Task(id=self.all_users.last_task_id,
                    name=name_task, time_end=time_end,
                    admin_task=admin_task.name_user,
                    users=[self.all_users.current_user.id], info=info)

        self.all_users.current_user.tasks.append(temp)
     
    @logg('Add subtask')
    def add_subtasks(self, name_task, time_end, task_id):
        """
        Add subtask.

        Keyword arguments:
            name_task (str) : name add task.
            time_end (:obj: 'datetime') :  deadline add task.
            task_id (int) : task id which add a subtask.

        Raises:
            ValueError : Name is None.
            ValueError : Time_end is None.
        """
        if name_task is None:
            raise ValueError('incorrect input, name is None')
        elif time_end is None:
            raise ValueError('incorrect input, deadline is None')
        elif task_id < 0 or task_id > self.all_users.last_task_id or self.search(task_id) is None:
            raise ValueError(
                'incorrect input, no task with id %s to add to it a subtask' % task_id)

        current_task = self.search(task_id)
        self.all_users.last_task_id += 1
        add_task = Task(id=self.all_users.last_task_id,
                        name=name_task, time_end=time_end,
                        admin_task=self.all_users.current_user.name_user,
                        users=[self.all_users.current_user.id])
        current_task.subtasks.append(add_task)

    @logg('Change task')
    def change(self, id, name_task=None, time_end=None,
               priority=None, tag=None, status=None):
        """
        Change task.

        Keyword arguments:
            id (int) : Id task.
            name_task (str) : new name task (default=None).
            time_end (:obj: 'datetime') : new deadline task
                (default=None).
            priority (int) : new priority task (default=None).
            tag (str) : new tag task (default=None).
            status (str) : new status task (default=None).
        Raises:
            ValueError : Incorrect period.
        """
        if priority < 0 or priority > 10 and priority:
            raise ValueError('incorrect period, priority must be [0,10]')

        edit_task = self.search(id)
        current_user_id = self.all_users.current_user.id

        for user_id in edit_task.users:

            self.all_users.switch_user(user_id)
            change_task = self.search(id=id, tasks=self.all_users.current_user.tasks)

            change_task.name = name_task or change_task.name

            if time_end is not None:
                change_task.time_end = time_end
            else:
                change_task.time_end = change_task.time_end

            change_task.task_priority = priority or change_task.task_priority
            change_task.tag = [tag] if tag is not None else None or change_task.tag

            if status is not None:
                change_task.status = _Status.INPROCESS

        self.all_users.switch_user(current_user_id)     

    @logg('Removed task')
    def remove(self, id):
        """
        Remove task.

        Keyword arguments:
            id (int) : id of the task to be remove.

        Raises:
            ValueError : Task is None.

        """
        if self.search(id) is None:
            raise ValueError('incorrect input, no task with id %s' % id)

        task = self.search(id)

        self.change_status(task, _Status.REMOVE)
        current_id = self.all_users.current_user.id

        if self.all_users.current_user.name_user != task.admin_task:

            task.users.remove(self.all_users.current_user.id)
            self.all_users.current_user.history.append(task)
            self.all_users.current_user.delete_task(task.id)

            for id_user in task.users:
                if id_user != current_id:
                    self.all_users.switch_user(id_user)
                    self.all_users.current_user.delete_user_from_task(
                        task.id, current_id)

        else:
            for user in task.users:
                self.all_users.switch_user(user)
                self.all_users.current_user.history.append(task)
                self.all_users.current_user.delete_task(id)

        self.all_users.switch_user(current_id)
    

    @logg('Search task')
    def search(self, id, tasks=None, search=False):
        """
        Search task with id.

        Keyword arguments:
            id (int) : Id task to search.
            tasks (:obj: 'Task', optional) : List to search
                task (default=None).
            search (:obj: 'bool', optional) : Is first deep
                method(default=None).
        Returns:
            Return value. Object Task for success, None otherwise.
        Raises:
            ValueError : ID is None.
            ValueError : id input is not a number.
            ValueError : incorrect input id (less 0)
        """
        if id is None:
            raise ValueError('incorrect id input(id a None)')
        elif not int(id):
            raise ValueError('incorrect id input(not a number)')
        elif id < 0 or id > self.all_users.last_task_id:
            raise ValueError('incorrect input, no task with id %s' % id)

        if tasks is None:
            tasks = self.collection_task

        for task in tasks:
            if task.id == id:
                return task
            elif task.subtasks:
                if self.search(id, task.subtasks, True):
                    return self.search(id, task.subtasks, True)
        if not search:
            return None

    @logg('show priority task')
    def task_search_by_priority(self, priority, all_task=None, result=[]):
        """Show task by priority.

        Keyword arguments:
            priority (int) : priority task to show.
            all_task (:obj: 'Task', optional) : List  task
                (default=None).
            check (:obj: 'bool', optional) : check for finding a
                task for a given tag (default=True).
        """
        if all_task is None:
            all_task = self.collection_task

        for task in all_task:
            if task.task_priority is not None and int(task.task_priority) == int(priority):
                result.append(task)
            elif task.subtasks:
                self.task_search_by_priority(priority, task.subtasks, result)

    @logg('show tag task')
    def task_search_by_tag(self, tag, list_task=None, result=[]):
        """Show task by tag.

        Keyword arguments:
            tag (str) : tag task to show.
            list_task (:obj: 'Task', optional) : List  task
                (default=None).
        """
        if list_task is None:
            list_task = self.collection_task
        for task in list_task:
            if tag in task.tag:
                result.append(task)
            elif task.subtasks:
                self.task_search_by_tag(tag, task.subtasks, result)
 
    @logg('add task tag')
    def add_task_tag(self, task_id, tag):
        """
        Add tag from task.

        Keyword arguments:
            task_id (id) : id task of which is to add a tag.
            tag (str) : tag.
        Raises:
            ValueError : The tag already exists in the task id.
            ValueError : Tag is None.

        """
        task = self.search(task_id)

        if tag in task.tag:
            raise ValueError(
                'The tag %s already exists in the task id %s' % (tag, task_id))
                
        elif tag is None:
            raise ValueError('Tag is None')

        else:
            task.tag.append(tag)

    @logg('change status task')
    def change_status(self, task, status=None, change_all_task=False):
        """
        Switch status task.

        Keyword arguments:
            task (Task) : Task to switch status.
            status (str) : Status to set(default=None).
            all_task (:obj: 'bool', optional) : switch status
                all task or one(default=False): 
        """
        task.status = status
        if task.subtasks and change_all_task is not False:
            for sub_from_task in task.subtasks:
                self.change_status(sub_from_task, status)

    @logg('complete task')
    def complete_task(self, id, list_task=None):
        """
        Complete task.

        Keyword arguments:
            id (int) : Id task to Complete.
            list_task (:obj: 'Task', optional) : Is first deep
                method (default=None).

        Raises:
            ValueError : no task with id
        """
        if self.search(id) is None:
            raise ValueError('incorrect input, no task with id %s' % id)

        if list_task is None:
            list_task = self.collection_task

        for task in list_task:
            if task.id == id:
                self.change_status(task, _Status.COMPLETE, True)
                if task.users:
                    for user_id in task.users:
                        users_from_this_task = self.all_users.search(user_id)
                        users_from_this_task.history.append(task)
                        users_from_this_task.delete_task(task.id)

            elif task.subtasks:
                self.complete_task(id, task.subtasks)

    @logg('Add user from task')
    def add_user_from_task(self, task_id, users_id, subtasks=None):
        """
        Add user from task.

        Keyword arguments:
            task_id (int) : Id task to which add user.
            users_id (int) : Id user to which add task.
            subtasks (:obj: 'Task', optional) : Is first deep
                method (default=None).

        Raises:
            ValueError : no task with id
            ValueError : no user with id
            ValueError : user have this task
        """

        if self.search(task_id) is None:
            raise ValueError('incorrect input, no task with id %s' % task_id)
        elif self.all_users.search(users_id) is None:
            raise ValueError('incorrect input, no user with id %s' % users_id)

        if subtasks is None:
            subtasks = self.collection_task

        for task in subtasks:
            if task.id == task_id:

                if users_id not in task.users:
                    task.users.append(users_id)
                else:
                    raise ValueError('user have task with id % s" % task_id')

                user = self.all_users.search(users_id)

                if task.subtasks:
                    self.add_user_from_subtask(users_id, task.subtasks)
                user.tasks.append(copy.deepcopy(task))
             
                return
            elif task.subtasks:
                self.add_user_from_task(task_id, users_id, task.subtasks)

    @logg('add user from task')
    def add_user_from_subtask(self, user_id, subtasks):
        """
        Add id user from subtask.

        Keyword arguments:
            users_id (int) : Id user to which add task.
            subtasks (:obj: 'Task', optional) : Is first deep
                method, all subtask from task.
        """
        for task in subtasks:
            if user_id not in task.users:
                task.users.append(user_id)
            if task.subtasks:
                self.add_user_from_subtask(user_id, task.subtasks)

    @logg('check from limit task')
    def check_from_limit(self, task_id, list_tasks=None):
        """
        Check task from limit.

        Keyword arguments:
            task_id (int) : Id task.
            list_tasks (:obj: 'Task', optional) : Is first deep
                method (default=None).

        Raises:
            ValueError : no task with id
        """
        if self.search(task_id) is None:
            raise ValueError('incorrect input, no task with id %s' % task_id)

        if list_tasks is None:
            list_tasks = self.collection_task
            
        for task in list_tasks:
            if task_id == task.id:
                if task.time_end < datetime.now():
                    self.change_status(task, _Status.LIMIT)
                    return
            elif task.subtasks:
                self.check_from_limit(task_id, task.subtasks)

    @logg('delete user from task')
    def delete_user_from_task(self, task_id, user_id):
        """
        Delete user from task.

        Keyword arguments:
            task_id (int) : Id task.
            user_id (int) : Id user.

        Raises:
            ValueError : Task is None (no task with id).
            ValueError : User is None (no user with id).
            ValueError : Current user not admin task.
        """

        if self.search(task_id) is None:
            raise ValueError('incorrect input, no task with id %s' % task_id)
        elif self.all_users.search(user_id) is None:
            raise ValueError('incorrect input, no user with id %s' % user_id)

        task = self.search(task_id)
        if task.id == task_id and not self.all_users.current_user.name_user == task.admin_task:
            raise ValueError('You dont admin task')

        else:

            current = self.all_users.current_user.id
            self.all_users.switch_user(user_id)
            self.all_users.current_user.delete_task(task_id)

            for id_user in task.users:
                if id_user != user_id:
                    self.all_users.switch_user(id_user)
                    self.all_users.current_user.delete_user_from_task(
                        task_id, user_id)
            self.all_users.switch_user(current)
      
    @logg('add period task')
    def add_period_task(self, task_id, current_user, period):
        """
        Add period task.

        Keyword arguments:
            task_id (int) : Id task to share.
            current_user(:obj: 'user') : user from whom to
                add period task
            period (:obj: 'datetime') : Period to repeat plan.
        """
        task = self.search(task_id)
        task.time_start = datetime.now()
        task.period = period
        current_user.planned_tasks.append(task)
 
    @logg('remove period task')
    def remove_period_task(self, task_id, current_user):
        """
        Remove period task.

        Keyword arguments:
            task_id (int) : Id task to share.
            current_user(:obj: 'user') : user from whom to
                remove period task
        """
        remove_task = self.search(task_id, current_user.planned_tasks)
        current_user.planned_tasks.remove(remove_task)
        remove_task.period = None

    @logg('change period task')
    def change_period_task(self, task_id, period):
        """
        Change period task.

        Keyword arguments:
            task_id (int) : Id task to share.
            period (:obj: 'datetime') : Change period to
                repeat plan.
        """
        task = self.search(task_id, self.all_users.current_user.planned_tasks)
        task.period = period

    @logg('switch admin task')
    def switch_admin_task(self, new_admin_id, id_task):
        """
        Switch admin from task.

        Keyword arguments:
            new_admin_id (int) : Id new admin task.
            task_id (int) : Id task.

        Raises:
            ValueError : Task is None.
            ValueError : New admin is None
        """
        change_task = self.search(id_task)
        new_admin = self.all_users.search(new_admin_id)
        if change_task is None or new_admin is None:
            raise ValueError('new admin or change task is None')
        if self.all_users.current_user.name_user == change_task.admin_task:
            change_task.admin_task = new_admin.name_user
