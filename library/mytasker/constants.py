from enum import Enum


class _Status(Enum):
    """Class Status. Stores all possible states of a task"""
    CREATE = 'Create'
    REMOVE = 'Remove'
    LIMIT = 'Limited'
    COMPLETE = 'Complete'
    INPROCESS = 'InProcess'
    RESTART = 'Restart'

    def __str__(self):
        return self.value


PRIORITIES = [(1, 'Low'), (2, 'Average'), (3, 'High'), (4,'Ultrahigh')]

PERIODS = [(1,'Hour'), (2, 'Day'), (3, 'Week'), (4, 'Month'), (5, 'Year')]
