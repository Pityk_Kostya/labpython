"""Default and primary database"""

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'db.sqlite3',
    }
}
INSTALLED_APPS = [
    'mytasker',
]


SECRET_KEY = '@*0ec$u&thi0%8fn8u=$3na7v5wxhi+prhszz*u^b@yba-)a(a'
USE_TZ = True
TIME_ZONE = 'Europe/Minsk'
