from django.db import models
from mytasker.constants import _Status, PRIORITIES
import re


class Task(models.Model):
    """
    Represents PlanTask instance
    Fields:
        task_name(str): name for task what will be created
        time_end(datetime): date when task will be done
        time_start(datetime): date when task create
        status(str): current task status
        tags(str): some grouping info
        task_info(str): description for task what will be created
        task_priority(dict):
            type of task:
            '1', Low: Low level priority
            '2', Average: Average level priority
            '3', High: High level priority
            '4', Ultrahigh: Ultrahigh level priority
        subtasks(Task): task in which subtasks container this task in
        owner(User): User instance which created this task
        plan(Plan): Plan instance what created this task
    """
    task_name = models.CharField(max_length=20, help_text='Enter name task')
    time_end = models.DateTimeField(blank=True, null=True, default=None)
    time_start = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=15, default=_Status.CREATE)
    tags = models.CharField(max_length=100, null=True,
                            blank=True, help_text='Some grouping info')
    task_info = models.CharField(
        max_length=20, help_text='Enter info about task')
    task_priority = models.PositiveSmallIntegerField(
        default=1, choices=PRIORITIES)
    subtasks = models.ForeignKey(
        'self',
        on_delete=models.CASCADE,
        related_name='sub',
        null=True
    )
    owner = models.ForeignKey(
        'User',
        null=True,
        on_delete=models.CASCADE,
        related_name='tasks',
        related_query_name='task'
    )
    plan = models.ForeignKey(
        'PlanTask',
        null=True,
        on_delete=models.CASCADE,
        related_name='plantask'
    )

    def __str__(self):
        return str(self.id) + '.' + str(self.task_name)

    def add_subtask(self, task):
        """
        Add task to subtasks container
        Args:
            task(Task): completed instance of Task
        """
        self.sub.add(task)
        self.save()

    def switch_status(self, status):
        """Switch status task"""
        self.status = status
        self.save()

    def complete_task(self):
        """Set status to COMPLETE of self task and subtasks"""
        self.status = _Status.COMPLETE
        self.save()
        for task in self.sub.all():
            task.complete_task()

    def delete_task(self):
        """Set status to REMOVE of self task and subtasks"""
        self.status = _Status.REMOVE
        self.save()
        for task in self.sub.all():
            task.delete_task()

    def revive(self):
        """Revive task from history and set status INPROCESS of self task and subtasks"""
        self.status = _Status.INPROCESS
        for task in self.sub.all():
            task.revive()
        self.save()
    
    def delete_from_history(self):
        """Delete task from database"""
        self.delete()