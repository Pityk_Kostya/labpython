from django.db import models
from mytasker.constants import PERIODS
from . import Task


class PlanTask(models.Model):
    """
    Represents PlanTask instance
    Fields:
        task_name(str): name for task what will be created
        info(str): description for task what will be created
        owner(User): User instance which created this plan
        time_end(datetime): date when task will be done
        period_type(dict): 
            type of plantask:
            '1', hour: create task after every N hour
            '2' day: create task after every N days
            '3', week: create task every given weekdays
            '4', month: create task every given day at given months
            '5', year: create task every given day at given year
        period(int): number (hours, days, weeks, months, years - specified in the period type)
    """

    task_name = models.CharField(max_length=20, help_text='Enter name task')
    info = models.CharField(max_length=100, default=str, help_text='Enter info task')
    owner = models.ForeignKey(
        'User',
        related_name='plans',
        null=True,
        on_delete=models.CASCADE)
    period_type = models.PositiveSmallIntegerField(choices=PERIODS, default=1)
    time_end = models.TimeField(null=True, blank=True)
    period = models.PositiveSmallIntegerField(default=1)

    def create_task(self):
        """Creates new periodic tasks if its time has come
        
        Return:
            New task object
        """
        new_task = Task(task_name=self.task_name,task_info=self.info, plan=self)
        new_task.save()
        return new_task

    def remove_task(self):
        """Remove task from database"""
        self.delete()
        
    def __str__(self):
        return self.task_name
