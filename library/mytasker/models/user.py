from django.db import models


class User(models.Model):
    """
    Represents User instance.

    Fields:
        name(str): name of user
        other_user_task(Task m2m relations): container of tasks for which this user is the performer
        my_tasks(Task m2m relations): container of user's tasks
        plans(Plan o2m relations): container of user's plans
       
    """
    name = models.CharField(max_length=20, default=str, unique=True)
    my_tasks = models.ManyToManyField('Task', related_name='user_tasks')
    other_user_task = models.ManyToManyField(
        'Task', related_name='other_user_task')

    def __str__(self):
        return self.name

    def add_task(self, task):
        """
        Add task instance to 'tasks' container

        Args:
            task(Task): Completed instance of Task
        """
        self.tasks.add(task)
        self.save()

    def add_task_from_user(self, task):
        """
        Attach another user's task to other_user_task
        Args:
             task: Completed instance of Task
        """
        self.my_tasks.add(task)
        self.other_user_task.add(task)
        self.save()

    def delete_task_from_user(self, task):
        """
        Detach another user's task from other_user_task
         Args:
             task: Completed instance of Task
        """
        self.my_tasks.remove(task)
        self.other_user_task.remove(task)
        self.save()

    def add_plan(self, plan):
        """
        Add plan instance to 'plans' container

        Args:
            plan(Plan): Completed instance of Plan
        """
        self.plans.add(plan)