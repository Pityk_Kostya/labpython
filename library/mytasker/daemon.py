from __future__ import print_function

import atexit
import errno
import os
import signal
import subprocess
import sys
import time
from datetime import datetime

from mytasker import (
    constants,
    config
)


class Daemon(object):

    def __init__(self, pidfile, stdin=os.devnull,
                 stdout=os.devnull, stderr=os.devnull,
                 home_dir='.', umask=0o22, verbose=1,
                 use_gevent=False, use_eventlet=False):
        self.stdin = stdin
        self.stdout = stdout
        self.stderr = stderr
        self.pidfile = pidfile
        self.home_dir = home_dir
        self.verbose = verbose
        self.umask = umask
        self.daemon_alive = True
        self.use_gevent = use_gevent
        self.use_eventlet = use_eventlet

    def daemonize(self):
        try:
            pid = os.fork()
            if pid > 0:
                sys.exit(0)
        except OSError as e:
            sys.stderr.write(
                "fork #1 failed: %d (%s)\n" % (e.errno, e.strerror))
            sys.exit(1)

        os.chdir(self.home_dir)
        os.setsid()
        os.umask(self.umask)

        try:
            pid = os.fork()
            if pid > 0:
                sys.exit(0)
        except OSError as e:
            sys.stderr.write(
                "fork #2 failed: %d (%s)\n" % (e.errno, e.strerror))
            sys.exit(1)

        atexit.register(
            self.del_pid)  # Make sure pid file is removed if we quit
        pid = str(os.getpid())
        open(self.pidfile, 'w+').write("%s\n" % pid)

    def del_pid(self):
        try:
            pid = int(open(self.pidfile, 'r').read().strip())
            if pid == os.getpid():
                os.remove(self.pidfile)
        except OSError as e:
            if e.errno == errno.ENOENT:
                pass
            else:
                raise

    def start(self, *args, **kwargs):
        # Check for a pidfile to see if the daemon already runs
        try:
            pf = open(self.pidfile, 'r')
            pid = int(pf.read().strip())
            pf.close()
        except IOError:
            pid = None
        except SystemExit:
            pid = None
        if pid:
            sys.exit(1)
        # Start the daemon
        self.daemonize()
        self.run(*args, **kwargs)

    def stop(self):
        subprocess.check_call(['notify-send', 'Stop'])
        # Get the pid from the pidfile
        pid = self.get_pid()

        if not pid:

            if os.path.exists(self.pidfile):
                os.remove(self.pidfile)

            return  # Not an error in a restart

        # Try killing the daemon process
        try:
            i = 0
            while 1:
                os.kill(pid, signal.SIGTERM)
                time.sleep(0.1)
                i = i + 1
                if i % 10 == 0:
                    os.kill(pid, signal.SIGHUP)
        except OSError as err:
            if err.errno == errno.ESRCH:
                if os.path.exists(self.pidfile):
                    os.remove(self.pidfile)
            else:
                print(str(err))
                sys.exit(1)

    def restart(self):

        self.stop()
        self.start()

    def get_pid(self):
        try:
            pf = open(self.pidfile, 'r')
            pid = int(pf.read().strip())
            pf.close()
        except IOError:
            pid = None
        except SystemExit:
            pid = None
        return pid

    def is_running(self):
        pid = self.get_pid()

        if pid is None:
            return False
        elif os.path.exists('/proc/%d' % pid):
            return True
        else:
            return False

    def run(self):

        raise NotImplementedError


class Notifications(Daemon):

    def __init__(self, tracker, pidfile='/tmp/tracker.pid'):
        super().__init__(pidfile)
        self.tracker = tracker
        self.config = config.Config()
        self.temp_file_path = self.config.get_setting("tempfile_path")

    def run(self):
        subprocess.check_call(['notify-send', 'Start'])
        while True:
            self._work()
            time.sleep(30)

    def _repeat(self, task):
        subprocess.check_call(['notify-send', task.name, 'Restart'])
        task.time_start = datetime.now()
        task.time_end = task.time_start + task.period
        task.status = constants._Status.RESTART
        self.tracker.last_task_id += 1
        task.id = self.tracker.last_task_id
        self.tracker.write(self.temp_file_path)
        os.rename(self.temp_file_path, self.tracker.path)

    def _limit(self, task):
        task.status = constants._Status.LIMIT
        subprocess.check_call(['notify-send', task.name, 'Deadline missed'])
        self.tracker.write(self.temp_file_path)
        os.rename(self.temp_file_path, self.tracker.path)

    def _work(self):
        for task in self.tracker.current_user.planned_tasks:
            if  datetime.now()>= task.time_end:
                self._repeat(task)

        for task in self.tracker.current_user.tasks:
            if task.period is None and task.status != constants._Status.LIMIT:
                if datetime.now() >= task.time_end:
                    self._limit(task)
    