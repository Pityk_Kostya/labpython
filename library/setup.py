import os

from django.core.management import execute_from_command_line
from setuptools import setup
from setuptools.command.install import install


class MyTaskerInstallCommand(install):
    def run(self):
        install.run(self)
        os.environ.setdefault("DJANGO_SETTINGS_MODULE", 'mytasker.database_settings.settings')
        execute_from_command_line(['manage.py', 'makemigrations', 'mytasker'])
        execute_from_command_line(['manage.py', 'migrate'])

setup(
    name='tasker',
    version='1.0',
    packages=[
        'mytasker', 
        'mytasker.models',
        'mytasker.database_settings'
    ],
    cmdclass={
        'install': MyTaskerInstallCommand,
    },
    data_files=['manage.py'],
    include_package_data=True
)
