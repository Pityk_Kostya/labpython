import unittest
from datetime import datetime
from mytasker.config import Config
from mytasker.management_task import ManageTask
from mytasker.management_user import ManageUser
from mytasker.constants import _Status


cfg = Config()
path = cfg.get_setting("tempfile_path")


class TestTask (unittest.TestCase):

    def setUp(self):
        self.user_list = ManageUser(path=path)
        self.user = self.user_list.add("first_test_name")
        self.my_tasks = ManageTask(
            self.user_list.current_user.tasks, self.user_list)

    def test_add_subtask(self):
        self.my_tasks.add('test', '25.10.2018 22:05', 'test_info')
        self.my_tasks.add_subtasks("test sub", '25.10.2018 22:05', 1)
        self.assertIsNotNone(self.my_tasks.search(int(2)))

    def test_change_task(self):
        self.my_tasks.add('test', '25.10.2018 22:05', 'test_change')
        self.my_tasks.change(
            1, 'change_test', '25.10.2018 22:05', 5, "InProcess", _Status.INPROCESS)
        self.assertEqual(self.my_tasks.search(1).name, 'change_test')

    def test_chack_correct_priority_when_change_task(self):
        self.my_tasks.add('test', '25.10.2018 22:05', 'test_change')
        with self.assertRaises(ValueError):
            self.my_tasks.change(1, 'change', priority=45)

    def test_search_task(self):
        self.my_tasks.add('test', '25.10.2018 22:05', 'test_change')
        self.assertEqual(self.my_tasks.search(1).name, 'test')

    def test_check_none_to_search_task(self):
        with self.assertRaises(ValueError):
            self.my_tasks.search(None)

    def test_check_not_int_id_to_search_task(self):
        with self.assertRaises(ValueError):
            self.my_tasks.search("sdds")

    def test_check_incorrect_id_to_search_task(self):
        with self.assertRaises(ValueError):
            self.my_tasks.search(-5)

    def test_search_tag_task(self):
        self.my_tasks.add('test', '25.10.2018 22:05', 'test_change')
        self.my_tasks.add_task_tag(1, 'hello')
        self.assertEqual(self.my_tasks.search(1).tag[0], 'hello')

    def test_add_tag_task(self):
        self.my_tasks.add('test', '25.10.2018 22:05', 'test_change')
        self.my_tasks.add_task_tag(1, 'hello')
        self.assertEqual(self.my_tasks.search(1).tag[0], 'hello')

    def test_exist_tag_task(self):
        with self.assertRaises(ValueError):
            self.my_tasks.add('test', '25.10.2018 22:05', 'test_change')
            self.my_tasks.add_task_tag(1, 'hello')
            self.my_tasks.add_task_tag(1, 'hello')

    def test_change_status(self):
        self.my_tasks.add('test', '25.10.2018 22:05', 'test_change')
        self.my_tasks.search(1).subtasks = []
        self.my_tasks.change_status(self.my_tasks.search(1), 'change status')
        self.assertEqual(self.my_tasks.search(1).status, 'change status')

    def test_complite_task(self):
        self.my_tasks.add('test', '25.10.2018 22:05', 'test_change')
        self.my_tasks.search(1).subtasks = []
        self.my_tasks.complete_task(1)
        self.assertEqual(self.my_tasks.search(1).status, _Status.COMPLETE)

    def test_check_for_limit_task(self):
        self.my_tasks.add('test', datetime.strptime('30.01.2018 05:50', "%d.%m.%Y %H:%M"), 'test_change')
        self.my_tasks.search(1).subtasks = []
        self.my_tasks.check_from_limit(1)
        self.assertEqual(self.my_tasks.search(1).status, _Status.LIMIT)

    def test_add_user_from_task(self):

        self.user_list.collection_users.remove(
            self.user_list.collection_users[1])
        self.my_tasks.add('test', '25.10.2018 22:05', 'test_change')
        self.my_tasks.search(1).subtasks = []
        self.user_list.add("secont")
        self.user_list.switch_user(1)
        self.my_tasks.add_user_from_task(1, 2)
        self.user_list.switch_user(2)
        self.my_tasks = ManageTask(
            self.user_list.current_user.tasks, self.user_list)
        self.assertIsNotNone(self.my_tasks.search(1))

    def test_check_exist_user_in_database(self):
        with self.assertRaises(ValueError):
            self.my_tasks.add_user_from_task(1, 1)

    def test_check_exist_task_in_database(self):
        self.my_tasks.add('test', '25.10.2018 22:05', 'test_change')
        with self.assertRaises(ValueError):
            self.my_tasks.add_user_from_task(1, 522)

    def test_add_period_task(self):
        self.my_tasks.add('test', '25.10.2018 22:05', 'test')
        self.my_tasks.add_period_task(1, self.user_list.current_user, '0.1.0')
        self.assertTrue(self.my_tasks.search(1).period, '0.1.0')

    def test_remove_period_task(self):
        self.my_tasks.add('test', '25.10.2018 22:05', 'test')
        self.my_tasks.add_period_task(1, self.user_list.current_user, '0.1.0')
        self.my_tasks.remove_period_task(1, self.user_list.current_user)
        self.assertEqual(self.user_list.current_user.planned_tasks, [])

    def test_change_period_task(self):
        self.my_tasks.add('test', '25.10.2018 22:05', 'test')
        self.my_tasks.add_period_task(1, self.user_list.current_user, '0.1.0')
        self.my_tasks.change_period_task(1, '5.5.12')
        self.assertEqual(self.my_tasks.search(1).period, '5.5.12')
