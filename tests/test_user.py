import unittest

from mytasker.config import Config
from mytasker.management_task import ManageTask
from mytasker.management_user import ManageUser

cfg = Config()
path = cfg.get_setting("tempfile_path")


class TestUser (unittest.TestCase):

    def setUp(self):
        self.user_list = ManageUser(path=path)
        self.user = self.user_list.add("first_test_name")
        self.my_tasks = ManageTask(
            self.user_list.current_user.tasks, self.user_list)

    def test_add_user(self):
        self.user_list.add("second_test_name")
        self.assertEqual(self.user_list.current_user.name_user,
                         'second_test_name')

    def test_change_user(self):
        self.user_list.change(1, "change")
        self.assertEqual(
            self.user_list.collection_users[0].name_user, 'change')

    def test_chack_name_is_none_in_change_user(self):
        with self.assertRaises(ValueError):
            self.user_list.change(1, None)

    def test_chack_not_correct_id_in_change_user(self):
        with self.assertRaises(ValueError):
            self.user_list.change(self.user_list.last_user_id + 1, 'change')

    def test_search_user(self):
        self.assertEqual(self.user_list.search(1).id, 1)

    def test_check_id_is_none_to_search(self):
        with self.assertRaises(ValueError):
            self.user_list.search(None)

    def tast_check_not_int_id_to_search_user(self):
        with self.assertRaises(ValueError):
            self.user_list.search('fgfgf')

    def tast_check_incorrect_id_to_search_task(self):
        with self.assertRaises(ValueError):
            self.user_list.search(self.user_list.last_user_id + 1)

    def test_switch_user(self):
        self.user_list.add('test_user')
        self.user_list.switch_user(2)
        self.assertEqual(self.user_list.current_user.name_user, 'test_user')

    def test_chack_correct_id_switch_user(self):
        with self.assertRaises(KeyError):
            self.user_list.switch_user(self.user_list.last_user_id + 1)
